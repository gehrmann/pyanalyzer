# Pyanalyzer

## Overview

It is a command line tool to communicate with automatic analyzer

BSD license, (c) 20017-2019 gehrmann [gehrmann.mail (at) gmail.com]

## Prerequisites

- Python 2.7
- pyserial

## Usage example

```shell
> ./pyanalyzer -h

usage: pyanalyzer [-h] [-v] [-s SRC_PATH] [-p PORT_PATH]
                  [--from-line FROM_LINE] [--to-line TO_LINE] [-d DST_PATH]
                  [-a APPEND] [--simulate SIMULATE]

Sends data to device through serial port.

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Raises logging level (For example: -v or -vv)
  -s SRC_PATH, --src-path SRC_PATH
                        Sets path to file with commands. Reads from STDIN if
                        not set.
  -p PORT_PATH, --port-path PORT_PATH
                        Sets path to serial port. Default is /dev/ttyUSB0 for
                        Linux, COM0 for Windows.
  --from-line FROM_LINE
                        Sets line to start from. Default is from the
                        beginning.
  --to-line TO_LINE     Sets line to stop. Default is to the end.
  -d DST_PATH, --dst-path DST_PATH
                        Sets path to write standard output. Writes to STDOUT
                        if not set.
  -a APPEND, --append APPEND
                        Enables appending data to previous content. Re-writes
                        output file if not set.
  --simulate SIMULATE
                        Simulates sending to device (for developing purposes)
```

Reads commands from file:

```shell
./pyanalyzer --src-path commands.txt
```

Reads commands from STDOUT:

```shell
echo heater off | ./pyanalyzer
```

Reads commands from STDOUT with a nice HEREDOC-syntax:

```shell
echo | ./pyanalyzer -v <<HEREDOC
	pump volume=0
HEREDOC
```

## FAQ

Q: Exception:

```
ImportError: No modules named serial
```

A: Install module pyserial from PIP repository:

```
> sudo pip2 install pyserial
```

## Available commands

Starts pump calibration loop:

```shell
pump calibrate
```

Pump will unload till the bottom is reached. This position will be saved as 0.

**Attention! In order to avoid mechanical damages of the pump, calibration process has always to be launched after automatic analyzer is switched on.**

------

Starts/stops pump loading/unloading:

```shell
pump [ load | unload | stop ]
```

------

Loads/unloads till the volume is in pump:

```shell
pump volume=<0.0-10.0>
```

------

Turns LED (built in pump) on/off:

```shell
light [ on | off ]
```

<u>If LED is on, the program will print out an output signal level of a photo-cell (built in pump).</u>

------

Starts/stops turning of pipe switch:

```shell
switch [ turn | stop ]
```

------

Sets pipe switch into a determined position:

```shell
switch position=<0-7>
```

------

Switches heater on/off:

```shell
heater [ on | off ]
```

------

Switches heater on until selected temperature is reached:

```
temperature=<float in celsium>
```

**Attention! Do not forget about liquid pumping during heating!**

## Typical usage (shell-script for Linux)

```shell
#!/bin/sh

echo | ./pyanalyzer -v <<HEREDOC
	# Must be always the first command
	pump calibrate

	# Loads water from input into cuvette
	switch position=1
	pump volume=10
	switch position=0
	pump volume=0

	# Loads chemicals from reservoir into cuvette
	switch position=2
	pump volume=1
	switch position=0
	pump volume=0

	# Pre-heats to 40grad
	temperature=40
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	pump volume=10
	pump volume=0
	heater off

	# Loads mix into pump
	pump volume=10
	light on
	light off

	# Unloads mix into the waste
	switch position=3
	pump volume=0
HEREDOC
```
