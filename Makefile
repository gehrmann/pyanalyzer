build:
	rm -rf dist build pyanalyzer.egg-info
	python setup.py bdist_wheel

install:
	sudo pip2 install dist/pyanalyzer-1.0-py2-none-any.whl

remove:
	sudo pip2 uninstall pyanalyzer-1.0-py2-none-any.whl

clean:
	rm -rf dist build pyanalyzer.egg-info
