#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:
# (c) gehrmann
#
# To build a wheel package:
#  > rm -rf dist build pyanalyzer.egg-info; python setup.py bdist_wheel
#
# To install a wheel package:
#  # sudo pip2 install dist/pyanalyzer-1.0-py2-none-any.whl
#
# To remove a wheel package:
#  # sudo pip2 uninstall dist/pyanalyzer-1.0-py2-none-any.whl
#

import os
import setuptools

long_description = '''Command line tool to communicate with automatic analyzer.
'''

setuptools.setup(
	name='pyanalyzer',
	version='1.0',
	author='Vladyslav Savchenko',
	author_email='gehrmann.apps@gmail.com',
	description=long_description.splitlines()[0],
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://bitbucket.org/gehrmann/pyanalyzer/',
	# install_requires=['serial'],
	modules=['serial'],
	scripts=['pyanalyzer'],
	data_files=[[path, [os.path.join(path, xx) for xx in files]] for path, directories, files in os.walk('share') if files],
	classifiers=[
		'Programming Language :: Python :: 2',
		'License :: OpenSource :: GPLv3 License',
		'Operating System :: OS Independent',
	],
)
